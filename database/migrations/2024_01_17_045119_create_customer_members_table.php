<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_members', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('customer_id');
            $table->string('name');
            $table->decimal('rate', 5,2);
            $table->integer("periode");
            $table->integer("avg_ip");
            $table->decimal("avg_fcr", 4,2);
            $table->integer("qty");
            $table->decimal("avg_profit", 25,4);
            $table->string("lat");
            $table->string("long");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_members');
    }
};
