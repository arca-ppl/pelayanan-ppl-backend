<?php

namespace Database\Seeders;

use App\Models\Customer;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            // GREEN CUSTOMER
            [
                'name' => "PT MPU",
                'rate' => 100,
            ],
            [
                'name' => "PT MKU",
                'rate' => 95,
            ],
            [
                'name' => "PT MJU",
                'rate' => 90,
            ],
            // ORANGE CUSTOMER
            [
                'name' => "PT GPS",
                'rate' => 73,
            ],
            [
                'name' => "PT MJR",
                'rate' => 80,
            ],
            [
                'name' => "PT BTB",
                'rate' => 78,
            ],
            // RED CUSTOMER
            [
                'name' => "PT BRU",
                'rate' => 50,
            ],
            [
                'name' => "PT AIL",
                'rate' => 46,
            ],
            [
                'name' => "PT SAW",
                'rate' => 60,
            ],

        ];

        foreach ($data as $each) {
            Customer::create([
                'name' => $each['name'],
                'rate' => $each['rate'],
                'periode' => fake("id_ID")->numberBetween(50, 80),
                'lat' => fake("id_ID")->latitude(-6, -8), 
                'long' => fake("id_ID")->longitude(106, 112)
            ]);
        }
    }
}
