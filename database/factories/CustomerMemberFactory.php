<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\CustomerMember>
 */
class CustomerMemberFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            "customer_id" => fake()->numberBetween(1, 9),
            "name" => fake("id_ID")->firstName(),
            "rate" => fake()->numberBetween(40, 100),
            "periode" => fake()->numberBetween(40, 80),
            "avg_ip" => fake()->numberBetween(300, 400),
            "avg_fcr" => fake()->randomFloat(2, 1, 2),
            "qty" => fake()->numberBetween(1000, 10000),
            "avg_profit" => fake()->numberBetween(1000, 5000),
            'lat' => fake("id_ID")->latitude(-6, -8), 
            'long' => fake("id_ID")->longitude(106, 112)
        ];
    }
}
