<?php

namespace App\Http\Controllers;

use App\Models\CustomerMember;
use Illuminate\Http\Request;

class CustomerMemberController extends Controller
{
    public function index()
    {
        return CustomerMember::orderBy("rate", 'desc')->with(['parent'])->get();
    }
}
